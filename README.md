# Title Similarity Checker

## Description
Title Similarity Checker is a Python script designed to interface with Jellyfin's API to compute the similarity score between movie titles and file names. This tool is particularly useful for managing and organizing movie libraries, ensuring consistency in naming conventions. It generates a tab-separated CSV file where the first field indicates the similarity percentage between a movie title and its corresponding file name.

## Features
- Automated querying of Jellyfin API for movie titles and file names.
- Calculation of similarity scores to aid in data organization.
- Output in CSV format for easy post-processing and analysis.

## Installation

### Requirements
- Python 3.x
- Libraries: `requests`, `json`, `sys`, `os`, `configparser`
- Access to a Jellyfin server with an API token.

### Steps
1. Clone the repository: `git clone https://gitlab.com/tzulux/title-similarity-checker.git`
2. Install the necessary Python libraries: `pip3 install scikit-learn requests configparser`

### Configuration File Setup
You have two options for setting up the `TitleSimilarityChecker.conf` configuration file:

**Option 1: Interactive Mode**
- If you run the script without a `.conf` file, it will switch to interactive mode.
- The script will prompt you for any missing parameters.
- After the initial setup, these settings will be saved in the `TitleSimilarityChecker.conf` file.
- On subsequent script runs, it will operate in unattended mode using this configuration file.

**Option 2: Manual Configuration**
- Alternatively, if you already have all the necessary parameters, you can manually create and edit the `TitleSimilarityChecker.conf` file in the project directory.
- Use the following format for the configuration file:

   ```ini
   [DEFAULT]
   server_ip = your_jellyfin_server_ip
   server_port = your_jellyfin_server_port
   auth_token = your_jellyfin_api_token
   user_id = your_jellyfin_user_id
   item_types = movies (or other types as needed)
   library_id = your_jellyfin_library_id
   csv_suffix = desired_suffix_for_csv_file
   ```

- With the `.conf` file in place, the script will run in an unattended mode, using the provided configuration.

## Usage
Run the script using `python TitleSimilarityChecker.py`. The script outputs a CSV file named based on your configuration, containing similarity scores. For the first run or for changing configuration, use `python TitleSimilarityChecker.py -i` to enter interactive mode.


## Support
For support, questions, or contributions, please open an issue on the GitLab repository.

## Roadmap
Future enhancements may include:
- Support for additional media types (tvshows).
- Improved algorithm for similarity checking.
- Identification of titles with a similarity index lower than 100% (by querying [TMDB](https://www.themoviedb.org) via API).

## Contributing
Contributions to Title Similarity Checker are welcome. Please ensure that your code adheres to the project's style and standards.

## Authors and Acknowledgment
Created by Lucio De Carli. Thanks to all contributors who have helped in the development of this project.

## License
This project is open source and available under the [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1) license.

## Project Status
The project is actively maintained. Any updates or changes will be documented in the repository's commit history.
