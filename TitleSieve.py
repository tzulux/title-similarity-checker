#!/usr/bin/env python3
# ----------------------------------------------------------------
# TitleSieve.py
# Version: 0.1.5
# This script takes as input a csv file 
# where the first field indicates the percentage of similarity
# between the next two text fields (Movie Title and File Name)
# and uses it to query the TMDB API to identify the correct titles
# ----------------------------------------------------------------
#  This work is licensed under CC BY 4.0

import os
import re
import csv
import time
import json
import datetime
import requests
import unidecode
import argparse
import configparser

config_filename = "TitleSieve.conf"

# Text to inform the user when default values are being used.
startup_notify = f" (settings from {config_filename})"

def read_config():
    config = configparser.ConfigParser()
    if os.path.exists(config_filename):
        config.read(config_filename)
        return config
    else:
        return None

def save_config(config):
    with open(config_filename, 'w') as configfile:
        config.write(configfile)

# Check if all configuration variables are present
def is_config_complete(config):
    required_keys = ['csv_input_file', 'server_domain', 'auth_token', 'csv_output_suffix']
    # Check if each key is present and has a non-empty value.
    return all(key in config['DEFAULT'] and config['DEFAULT'][key].strip() for key in required_keys)

# Output directory where the .csv files will be saved.
output_dir = 'Results'
if not os.path.exists(output_dir):
    # If it does not exist, create it.
    os.makedirs(output_dir)

# Text to inform the user when default values are being used.
startup_notify = f" (settings from {config_filename})"

# Setup of the argparse parser.
parser = argparse.ArgumentParser(description="Title Sieve")
parser.add_argument('-i', '--interactive', action='store_true', help='Start in interactive mode.')
args = parser.parse_args()

# Function to prompt the user for input with a default value.
def input_with_default(prompt, default):
    prompt += f" [{default}]: " if default else ": "
    user_input = input(f"{prompt}").strip()
    return user_input if user_input else default

# INTERACTIVELY asks the user for the information needed to run the script.
def get_user_input(config=None):
    if config is None:
        config = configparser.ConfigParser()

    # Take existing values as defaults (if present).
    default_csv_input_file = config['DEFAULT'].get('csv_input_file', 'TEST_0010.csv')            # 
    default_server_domain = config['DEFAULT'].get('server_domain', 'themoviedb.org')             # 
    default_auth_token = config['DEFAULT'].get('auth_token', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') # 
    default_csv_output_suffix = config['DEFAULT'].get('csv_output_suffix', 'TitlesSieved')       # 

    csv_input_file = input_with_default("Enter the INPUT .csv file name", default_csv_input_file)
    server_domain = input_with_default("Enter the TMDB API domain", default_server_domain)
    auth_token = input_with_default("Enter your TMDB API token", default_auth_token)
    csv_output_suffix = input_with_default("Enter a suffix for the OUTPUT .csv file name", default_csv_output_suffix)

    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'csv_input_file': csv_input_file,
        'server_domain': server_domain,
        'auth_token': auth_token,
        'csv_output_suffix': csv_output_suffix
    }
    return config

# Reads the configuration file (or obtains the information from the user).
config = read_config()
if args.interactive or config is None or not is_config_complete(config):
    startup_notify = ""
    config = get_user_input(config)
    save_config(config)

# Use the settings from the configuration file.
csv_input_file = config['DEFAULT']['csv_input_file']
server_domain = config['DEFAULT']['server_domain']
auth_token = config['DEFAULT']['auth_token']
csv_output_suffix = config['DEFAULT']['csv_output_suffix']

# Composes the CSV file name with the suffix and the current date.
current_datetime = datetime.datetime.now().strftime("%Y-%m-%d_%H%M")
csv_output_file = f"{output_dir}/{csv_output_suffix}_{current_datetime}.csv"

def extract_title_and_year(media_source_name):
    # Use a regular expression to extract the title and year
    match = re.search(r'(.*)\s+\((\d{4})\)$', media_source_name)
    if match:
        return match.group(1), match.group(2)
    else:
        return media_source_name, 'Unknown'

def query_tmdb(title, year, server_domain, auth_token):
    url = f"https://api.{server_domain}/3/search/movie"
    headers = {
        "Authorization": f"Bearer {auth_token}",
        "accept": "application/json"
    }
    params = {
        "query": title,
        "include_adult": "true",
        "language": "it-IT",
        "page": "1",
        "year": year
    }

    response = requests.get(url, headers=headers, params=params)
    return response

def make_request_with_rate_limiting(title, year, server_domain, auth_token):
    MAX_REQUESTS_PER_SECOND = 50
    WAIT_SECONDS = 1 / MAX_REQUESTS_PER_SECOND

    retry_delay = 1  # Starts with a 1 second retry delay
    max_retries = 5  # Maximum number of retry attempts

    for _ in range(max_retries):
        response = query_tmdb(title, year, server_domain, auth_token)
        if response.status_code == 200:
            return response.json()
        elif response.status_code == 429:
            print(f"Received error 429, please wait {retry_delay} seconds before trying again...")
            time.sleep(retry_delay)
            retry_delay *= 2  # It increases the delay exponentially
        else:
            return f"Error: {response.status_code}"

        time.sleep(WAIT_SECONDS)  # Respect the rate limit

    return "Error: Maximum number of attempts exceeded"

def preprocessTitle(text):
    # Replacement of the characters ? and :
    text = re.sub(r'(\w)([?:%])', r'\1 -', text)  # Replace `?`, `:` and `%` that immediately follow a word.
    text = re.sub(r' ([?:%])', r' -', text)       # Replace `?`, `:` and `%` preceded by a space.

    # Dictionary defining substitutes for accented characters.
    accent_replacements = {
        'à': 'a', 
        'è': 'e', 
        'ì': 'i', 
        'ò': 'o', 
        'ù': 'u'
    }
    # Replacement of accented characters at the end of a word (replacing them with the corresponding character followed by an apostrophe).
    for accented_char, replacement in accent_replacements.items():
        text = re.sub(f'{accented_char}(\\b)', f'{replacement}\'', text)

    # Normalization of the text to lowercase.
    return text.lower()

def create_movie_url(server_domain, movie_id, title):
    # Transliterate non-ASCII characters and convert text to lowercase
    clean_title = unidecode.unidecode(title).lower()

    # Replace spaces and non-alphanumeric characters with hyphens
    clean_title = re.sub(r'[^a-z0-9]', '-', clean_title)

    # Remove multiple hyphens or hyphens at the beginning or end of the title
    clean_title = re.sub(r'-+', '-', clean_title).strip('-')

    return f"https://www.{server_domain}/movie/{movie_id}-{clean_title}"

def format_execution_time(total_seconds):
    minutes, seconds = divmod(total_seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)

    if total_seconds < 60:
        return f"Script executed in {total_seconds:.2f} seconds."
    elif total_seconds < 3600:
        return f"Script executed in {int(minutes)} minutes, {seconds:.2f} seconds."
    elif total_seconds < 86400:
        return f"Script executed in {int(hours)} hours, {int(minutes)} minutes, {seconds:.2f} seconds."
    else:
        return f"Script executed in {int(days)} days, {int(hours)} hours, {int(minutes)} minutes, {seconds:.2f} seconds."

# Start tracking time
start_time = time.time()

# Inform the user that the processing has started.
print(f"Processing in progress{startup_notify}...")

total_titles = 0
imperfect_titles_counter = 0

# Open and read the CSV file
with open(csv_input_file, newline='', encoding='utf-8') as csvinputfile, \
     open(csv_output_file, 'w', newline='', encoding='utf-8') as csvoutputfile:
    
    csv_reader = csv.reader(csvinputfile, delimiter=',')
    next(csv_reader)  # Skip header if present

    csv_writer = csv.writer(csvoutputfile, delimiter=',')
    csv_writer.writerow(['File Name Title', 'Year', 'TMDB Title', 'TMDB Date', 'TMDB URL'])

    for row in csv_reader:
        total_titles += 1
        similarity_str, title, media_source_name = row
        # Convert similarity to a decimal number and check if it is less than 100
        similarity = float(similarity_str.replace(',', '.'))
        if similarity < 100:
            imperfect_titles_counter += 1
            extracted_title, extracted_year = extract_title_and_year(media_source_name)
            print(f"File Name Title: {extracted_title} ({extracted_year})")

            # Here we use extracted_title and extracted_year for TMDB API call
            response = make_request_with_rate_limiting(extracted_title, extracted_year, server_domain, auth_token)
            for movie in response['results']:
                title = movie['title']
                original_title = movie['original_title']
                movie_id = movie['id']
                release_date = movie['release_date']
                #print(f"TMDB          Title: {title}")
                #print(f"TMDB Original Title: {original_title}")
                #print(f"TMDB   Release Date: {release_date}")
                #print(f"TMDB             ID: {movie_id}")
                url = create_movie_url(server_domain, movie_id, original_title)
                #print(f"TMDB            URL: {url}")
                
                # Transliterate non-ASCII characters and convert text to lowercase
                normalized_extracted_title = unidecode.unidecode(extracted_title).lower()
                normalized_title = preprocessTitle(title)

                # Write the processed line to the output file
                csv_writer.writerow([normalized_extracted_title, extracted_year, normalized_title, release_date, url])
            #DEBUG print(json.dumps(response, indent=4))
            #print()

# Calculate the total execution time
end_time = time.time()
total_time = end_time - start_time

print(f"Total titles in input: {total_titles}")
print(f"Titles processed: {imperfect_titles_counter}")
print(f"Output list saved in: '{csv_output_file}'")
print(format_execution_time(total_time))
