#!/usr/bin/env python3
# -------------------------------------------------------------
# TitleSimilarityChecker.py
# Version: 0.4.6
# This script queries Jellyfin via API and outputs a CSV file
# where the first field indicates the percentage of similarity
# between the next two text fields (Movie Title and File Name)
# -------------------------------------------------------------
#  This work is licensed under CC BY 4.0

import os
import re
import csv
import sys
import json
import time
import datetime
import requests
import argparse
import configparser
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.metrics.pairwise import cosine_similarity

config_filename = "TitleSimilarityChecker.conf"

# Define a similarity threshold
SIMILARITY_THRESHOLD = 0.9999

def read_config():
    config = configparser.ConfigParser()
    if os.path.exists(config_filename):
        config.read(config_filename)
        return config
    else:
        return None

def save_config(config):
    with open(config_filename, 'w') as configfile:
        config.write(configfile)

# Check if all configuration variables are present
def is_config_complete(config):
    required_keys = ['server_ip', 'server_port', 'auth_token', 'user_id', 'item_types', 'library_id', 'csv_suffix']
    # Check if each key is present and has a non-empty value.
    return all(key in config['DEFAULT'] and config['DEFAULT'][key].strip() for key in required_keys)

# Output directory where the .csv files will be saved.
output_dir = 'Results'
if not os.path.exists(output_dir):
    # If it does not exist, create it.
    os.makedirs(output_dir)

# Text to inform the user when default values are being used.
startup_notify = f" (settings from {config_filename})"

# Setup of the argparse parser.
parser = argparse.ArgumentParser(description="Title Similarity Checker")
parser.add_argument('-i', '--interactive', action='store_true', help='Start in interactive mode.')
args = parser.parse_args()

# Function to prompt the user for input with a default value.
def input_with_default(prompt, default):
    prompt += f" [{default}]: " if default else ": "
    user_input = input(f"{prompt}").strip()
    return user_input if user_input else default

# Function to provide a menu-type input (the "options" parameter is a key-value dictionary).
def menu_selection(prompt, options, default=None):
    print() # Inserts an empty line as a spacer from the previous output.
    print("Choose from the following options:")
    for i, (key, value) in enumerate(options.items(), 1):
        print(f"{i}. {value}")

    if default:
        default_value = options.get(default, None)
        prompt += f" [{default_value}]: " if default_value else ": "
    else:
        prompt += ": "

    while True:
        user_input = input(prompt).strip()
        if not user_input and default_value is not None:
            return default

        try:
            choice = int(user_input)
            if 1 <= choice <= len(options):
                selected_key = list(options.keys())[choice - 1]
                return selected_key
            else:
                print("Invalid selection. Please try again.")
        except ValueError:
            print("Enter a valid number.")

# Dynamic acquisition of the USERS dictionary directly from Jellyfin via API.
def get_user_id_options(server_ip, server_port, auth_token):
    url = f"http://{server_ip}:{server_port}/Users"
    headers = {
        "accept": "application/json",
        "x-emby-token": auth_token
    }
    
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        return {item['Id']: item['Name'] for item in data}
    else:
        raise Exception(f"API call failed with status code {response.status_code}")

# Dynamic acquisition of the LIBRARIES dictionary directly from Jellyfin via API.
def get_library_id_options(server_ip, server_port, user_id, auth_token, filter_type="movies"):
    url = f"http://{server_ip}:{server_port}/Items?userId={user_id}"
    headers = {
        "accept": "application/json",
        "x-emby-token": auth_token
    }
    
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        # Filter objects based on the CollectionType field.
        filtered_items = [item for item in data['Items'] if item.get('CollectionType') == filter_type]
        return {item['Id']: item['Name'] for item in filtered_items}
    else:
        raise Exception(f"API call failed with status code {response.status_code}")


# INTERACTIVELY asks the user for the information needed to run the script.
def get_user_input(config=None):
    if config is None:
        config = configparser.ConfigParser()

    # Take existing values as defaults (if present).
    default_server_ip = config['DEFAULT'].get('server_ip', '192.168.0.28')                       # 
    default_server_port = config['DEFAULT'].get('server_port', '8096')                           # 
    default_auth_token = config['DEFAULT'].get('auth_token', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') # 
    default_user_id = config['DEFAULT'].get('user_id', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx')       # 
    default_item_types = config['DEFAULT'].get('item_types', 'movies')                           # 
    default_library_id = config['DEFAULT'].get('library_id', 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx') # 
    default_csv_suffix = config['DEFAULT'].get('csv_suffix', 'TitleScore')                       # 

    server_ip = input_with_default("Enter the server IP", default_server_ip)
    server_port = input_with_default("Enter the server port", default_server_port)
    auth_token = input_with_default("Enter the authentication token", default_auth_token)

    # Selection of the USER from the USERS dictionary.
    user_id_options = get_user_id_options(server_ip, server_port, auth_token)
    if not default_user_id:
        default_user_id = next(iter(user_id_options.values()), None)
    user_id = menu_selection("Select the user", user_id_options, default_user_id)

    # Selection of the MEDIA TYPE from the MEDIA TYPES dictionary.
    #[TODO] item_types_options = {"movies": "movies", "tvshows": "tvshows"}
    #[TODO] if not default_item_types:
    #[TODO]     default_item_types = next(iter(item_types_options.values()), None)
    #[TODO] item_types = menu_selection("Select the type of item.", item_types_options, default_item_types)
    item_types = default_item_types

    # Selection of the LIBRARY from the LIBRARIES dictionary.
    library_id_options = get_library_id_options(server_ip, server_port, user_id, auth_token, item_types)
    if not default_library_id:
        default_library_id = next(iter(library_id_options.values()), None)
    library_id = menu_selection("Select the library", library_id_options, default_library_id)

    # Asks the user for the suffix for the CSV file name.
    csv_suffix = input_with_default("Enter a suffix for the CSV file name", default_csv_suffix)

    config = configparser.ConfigParser()
    config['DEFAULT'] = {
        'server_ip': server_ip,
        'server_port': server_port,
        'auth_token': auth_token,
        'user_id': user_id,
        'item_types': item_types,
        'library_id': library_id,
        'csv_suffix': csv_suffix
    }
    return config


# Reads the configuration file (or obtains the information from the user).
config = read_config()
if args.interactive or config is None or not is_config_complete(config):
    startup_notify = ""
    config = get_user_input(config)
    save_config(config)

# Use the settings from the configuration file.
server_ip = config['DEFAULT']['server_ip']
server_port = config['DEFAULT']['server_port']
auth_token = config['DEFAULT']['auth_token']
user_id    = config['DEFAULT']['user_id']
item_types = config['DEFAULT']['item_types']
library_id = config['DEFAULT']['library_id']
csv_suffix = config['DEFAULT']['csv_suffix']


# Composes the CSV file name with the suffix and the current date.
current_datetime = datetime.datetime.now().strftime("%Y-%m-%d_%H%M")
csv_filename = f"{output_dir}/{csv_suffix}_{current_datetime}.csv"


def call_jellyfin_api(server_ip, server_port, user_id, auth_token, library_id):
    url = f"http://{server_ip}:{server_port}/Users/{user_id}/Items?parentId={library_id}&fields=MediaSources"
    headers = {
        "accept": "application/json",
        "x-emby-token": auth_token
    }
    response = requests.get(url, headers=headers)
    if response.status_code == 200:
        data = response.json()
        filtered_data = [
            {
                "ItemName": item["Name"],
                "ProductionYear": item.get("ProductionYear"),
                "MediaSourceName": [ms["Name"] for ms in item.get("MediaSources", [])]
            }
            for item in data["Items"]
        ]
        return filtered_data
    else:
        raise Exception(f"API call failed with status code {response.status_code}")

def preprocessTitle(text):
    # Replacement of the characters ? and :
    text = re.sub(r'(\w)([?:%])', r'\1 -', text)  # Replace `?`, `:` and `%` that immediately follow a word.
    text = re.sub(r' ([?:%])', r' -', text)       # Replace `?`, `:` and `%` preceded by a space.

    # Dictionary defining substitutes for accented characters.
    accent_replacements = {
        'à': 'a', 
        'è': 'e', 
        'ì': 'i', 
        'ò': 'o', 
        'ù': 'u'
    }
    # Replacement of accented characters at the end of a word (replacing them with the corresponding character followed by an apostrophe).
    for accented_char, replacement in accent_replacements.items():
        text = re.sub(f'{accented_char}(\\b)', f'{replacement}\'', text)

    # Normalization of the text to lowercase.
    return text.lower()

def preprocessFileName(text):
    # Remove text in square brackets following the year enclosed in parentheses.
    text = re.sub(r'\((\d{4})\)\s*\[.*?\].*', r'(\1)', text)

    # Remove non-relevant text to reduce differences between strings.
    text = re.sub(r"-ITA.*", "", text)
    text = re.sub(r"-MYN.*", "", text)
    text = re.sub(r"-LAT.*", "", text)
    text = re.sub(r"-UND.*", "", text)
    text = re.sub(r"-MUTO.*", "", text)
    # Normalization of the text to lowercase.
    return text.lower()

def calculate_cosine_similarity(str1, str2):
    # The vectorizer will consider bigrams (sequences of 2 characters) and trigrams (sequences of 3 characters).
    vectorizer = CountVectorizer(analyzer='char', ngram_range=(2, 3))

    # Transforming str1 and str2 into numerical vectors using CountVectorizer.
    # `fit_transform` fits the model to the data and then transforms it:
    # it learns the vocabulary of the two strings and then converts the strings into matrices of n-gram counts.
    vectors = vectorizer.fit_transform([str1, str2])

    # Cosine Similarity Calculation:
    # `cosine_similarity` is a function from scikit-learn that calculates the cosine similarity between two arrays.
    # Here, it calculates the similarity between the vectors (n-gram count matrices) of the two strings.
    # `vectors[0:1]` and `vectors[1:2]` respectively select the vector of the first and second strings for the calculation.
    cosine_sim = cosine_similarity(vectors[0:1], vectors[1:2])

    # `cosine_similarity` returns a similarity matrix, where element [0,0] represents the cosine similarity between str1 and str2.
    # The returned value is a number between 0 and 1, where 1 indicates a perfect match (maximum similarity) and 0 indicates no similarity.
    return cosine_sim[0,0]

def format_execution_time(total_seconds):
    minutes, seconds = divmod(total_seconds, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)

    if total_seconds < 60:
        return f"Script executed in {total_seconds:.2f} seconds."
    elif total_seconds < 3600:
        return f"Script executed in {int(minutes)} minutes, {seconds:.2f} seconds."
    elif total_seconds < 86400:
        return f"Script executed in {int(hours)} hours, {int(minutes)} minutes, {seconds:.2f} seconds."
    else:
        return f"Script executed in {int(days)} days, {int(hours)} hours, {int(minutes)} minutes, {seconds:.2f} seconds."

# Start tracking time
start_time = time.time()

# Inform the user that the processing has started.
print(f"Processing in progress{startup_notify}...")

# Calculates the number of decimal places in SIMILARITY_THRESHOLD
threshold_str = f"{SIMILARITY_THRESHOLD:.15f}"  # Convert to string with a fixed number of decimal places
decimal_places = len(threshold_str.split('.')[1].rstrip('0'))  # Count the digits after the decimal point

#print("[DEBUG] Forced program termination."); sys.exit(1) # DEBUG

# Call Jellyfin API and process data
input_data = call_jellyfin_api(server_ip, server_port, user_id, auth_token, library_id)

# Data processing.
with open(csv_filename, 'w', newline='', encoding='utf-8') as csv_file:
    csv_writer = csv.writer(csv_file)
    csv_writer.writerow(['Similarity', 'Title', 'MediaSourceName'])
    
    total_titles = 0
    imperfect_titles_counter = 0

    for item in input_data:
        item_name = item['ItemName']
        production_year = item['ProductionYear']
        is_imperfect = False  # Flag to indicate whether the item has imperfect similarity

        for media_source_name in item['MediaSourceName']:
            formatted_item_name = f"{item_name} ({production_year})"
            strTitle = preprocessTitle(formatted_item_name)
            strFileName = preprocessFileName(media_source_name)
            similarity = calculate_cosine_similarity(strTitle, strFileName)
            similarity = min(similarity, 1)  # Limit similarity to 1
            similarity = round(similarity, 15)  # Round to 15 decimal places
            total_titles += 1

            # If the similarity is < 1, mark the item as imperfect
            if similarity < SIMILARITY_THRESHOLD:
                is_imperfect = True

            # Write the results to the CSV file
            similarity_str = f"{similarity * 100:.{decimal_places}f}".replace('.', ',')
            csv_writer.writerow([similarity_str, strTitle, strFileName])

        # Increment the counter only if the item has at least one imperfect media source
        if is_imperfect:
            imperfect_titles_counter += 1

# Calculate the total execution time
end_time = time.time()
total_time = end_time - start_time

print(f"Processing completed.")
print(f"Total titles processed: {total_titles}")
print(f"Titles with a similarity index less than 100%: {imperfect_titles_counter}")
print(f"Complete list saved in: '{csv_filename}'")
print(format_execution_time(total_time))
